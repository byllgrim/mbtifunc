#!/usr/bin/env python3


import unittest
import mbtifunc


class  TestMBTIFunc ( unittest.TestCase ):

    type_func_expectations = {
        "INTP": "TiNeSiFe",
        "INTJ": "NiTeFiSe",
        "INFP": "FiNeSiTe",
        "INFJ": "NiFeTiSe",

        "ISTP": "TiSeNiFe",
        "ISTJ": "SiTeFiNe",
        "ISFP": "FiSeNiTe",
        "ISFJ": "SiFeTiNe",

        "ENTP": "NeTiFeSi",
        "ENTJ": "TeNiSeFi",
        "ENFP": "NeFiTeSi",
        "ENFJ": "FeNiSeTi",

        "ESTP": "SeTiFeNi",
        "ESTJ": "TeSiNeFi",
        "ESFP": "SeFiTeNi",
        "ESFJ": "FeSiNeTi",
        }

    def  test_type_to_function ( self ):
        for  type  in  self.type_func_expectations:
            expected = self.type_func_expectations [ type ]
            actual   = mbtifunc.type_to_functions ( type )
            msg      = type

            self.assertEqual ( expected, actual, msg )


    def  test_capitalization ( self ):
        expected = mbtifunc.type_to_functions ( "intp" )
        actual = mbtifunc.type_to_functions ( "INtP" )

        self.assertEqual ( expected, actual )


unittest.main()
