default:
	@echo make targets
	@cat Makefile | grep '.*:$$' | sed 's/://' | sed 's/^/  /'

run:
	./mbtifunc.py

check:
	mypy ./mbtifunc.py

test:
	./test_mbtifunc.py
