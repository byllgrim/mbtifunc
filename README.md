This script calculates the MBTI "functions" for a given type.

E.g. "INTP" -> "TiNeSiFe".

--------

The terminology used here for the four axes are:

1. "Attitude" (E/I)
2. "Irrational" Functions (S/N)
3. "Rational" Functions (T/F)
4. "Tactics" (J/P)

And also, "rationality",
where "ExxP" / "IxxJ" is irrational (dominant S/N),
and "ExxJ" / "IxxP" is rational (dominant T/F).
