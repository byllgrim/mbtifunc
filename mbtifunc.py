#!/usr/bin/env python3


def  calc_rationality ( type: str ) -> str:
    first_second = type[0] + type[3]

    if  first_second in ["EP", "IJ"]:
        return "rationality_irrational"
    elif  first_second in ["EJ", "IP"]:
        return "rationality_rational"
    else:
        raise ValueError ( "got '" + first_second + "'")


def  reverse_attitude ( attitude ):
    if attitude in ["E", "e"]:
        return "i"
    elif attitude in ["I", "i"]:
        return "e"
    else:
        raise ValueError ( "got '" + attitude + "'")


def  reverse_rational ( rational ):
    if rational == "T":
        return "F"
    elif rational == "F":
        return "T"
    else:
        raise ValueError ( "got '" + rational + "'")


def  reverse_irrational ( irrational ):
    if  irrational in ["N", "n"]:
        return "S"
    elif  irrational in ["S", "s"]:
        return "N"
    else:
        raise ValueError ( "got '" + irrational + "'")


def  calc_dominant_0 ( type: str ) -> str:
    rationality = calc_rationality ( type )

    if rationality == "rationality_irrational":
        return type[1].upper()
    elif rationality == "rationality_rational":
        return type[2].upper()
    else:
        raise ValueError ( "got '" + rationality + "'")


def  calc_dominant_1 ( type: str ) -> str:
    attitude = type[0].lower()

    return attitude


def  calc_auxiliary_0 ( type ):
    rationality = calc_rationality ( type )

    if rationality == "rationality_irrational":
        return type[2].upper()
    elif rationality == "rationality_rational":
        return type[1].upper()
    else:
        raise ValueError ( "got '" + rationality + "'")


def  calc_auxiliary_1 ( type ):
    attitude = type[0]

    return  reverse_attitude ( attitude )


def  calc_tertiary_0 ( type ):
    rationality = calc_rationality ( type )
    auxiliary_0 = calc_auxiliary_0 ( type )

    if rationality == "rationality_irrational":
        return reverse_rational ( auxiliary_0 )
    elif rationality == "rationality_rational":
        return reverse_irrational ( auxiliary_0 )
    else:
        raise ValueError ( "got '" + rationality + "'")


def  calc_tertiary_1 ( type ):
    return  calc_dominant_1 ( type )


def  calc_inferior_0 ( type ):
    rationality = calc_rationality ( type )
    dominant_0  = calc_dominant_0 ( type )

    if rationality == "rationality_irrational":
        return reverse_irrational ( dominant_0 )
    elif rationality == "rationality_rational":
        return reverse_rational ( dominant_0 )
    else:
        raise ValueError ( "got '" + rationality + "'")


def  calc_inferior_1 ( type ):
    return  calc_auxiliary_1 ( type )


def  calc_dominant ( type ):
    letter_0 = calc_dominant_0 ( type )
    letter_1 = calc_dominant_1 ( type )

    return  letter_0 + letter_1


def  calc_auxiliary ( type ):
    letter_0 = calc_auxiliary_0 ( type )
    letter_1 = calc_auxiliary_1 ( type )

    return  letter_0 + letter_1


def  calc_tertiary ( type ):
    letter_0 = calc_tertiary_0 ( type )
    letter_1 = calc_tertiary_1 ( type )

    return  letter_0 + letter_1


def  calc_inferior ( type ):
    letter_0 = calc_inferior_0 ( type )
    letter_1 = calc_inferior_1 ( type )

    return  letter_0 + letter_1


def  type_to_functions ( type: str ) -> str:
    type = type.upper()

    dominant  = calc_dominant  ( type )
    auxiliary = calc_auxiliary ( type )
    tertiary  = calc_tertiary  ( type )
    inferior  = calc_inferior  ( type )

    functions = dominant + auxiliary + tertiary + inferior

    return  functions
